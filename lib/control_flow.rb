# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |c| str.delete!(c) if c == c.downcase }
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 != 0
    str[str.length / 2]
  else
    str[str.length / 2 - 1] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char { |c| count += 1 if VOWELS.include?(c.downcase) }
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  i = 1

  while i <= num
    product *= i
    i += 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return "" if arr.empty?
  str = ""
  arr.each { |el| str << el.to_s && (str << separator unless el == arr[-1]) }
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = str.upcase
  i = 0
  while i < weird_str.length
    weird_str[i] = weird_str[i].downcase
    i += 2
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  reversed = []

  words.each do |word|
    if word.length >= 5
      reversed << word.reverse
    else
      reversed << word
    end
  end
  reversed.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []

  (1..n).each do |i|
    str = ""

    str += "fizz" if i % 3 == 0
    str += "buzz" if i % 5 == 0

    str.empty? ? array << i : array << str
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = arr.reverse
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  i = 2
  while i <= Math.sqrt(num).floor
    if num % i == 0
      return false
    end
    i += 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  i = 1

  while i <= num
    if num % i == 0
      result << i
    end
    i += 1
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  factors.select { |n| prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = arr.select { |n| n % 2 == 0 }
  odds = arr.reject { |n| n % 2 == 0 }
  evens.count == 1 ? evens[0] : odds[0]
end
